# Web-Service Specification

This document will describe the specification of _Request_ and _Response_ in _Restful Web-Service_. All format will follow the specification from [json-schema.org](https://json-schema.org/).

The specification will describe in `json` file and there will be schema for specification itself also.

## Terminology

Keyword | Definition
--------|-----------
_specification_ | document containt detail of web-service (the endpoint, the request body, the response body, URL query parmeter, header included, etc.)
_definition_ | _scheme_ used to describe the specification
_request_ | data sent from client to the server
_response_ | data received from the server to the client
_model_ | representation of object in the server or a resource from the server. Model can be form from database structure, query db, or data manipulation from server. You can find the detail of model referencing from the class diagram of _School Hub Admin App_

## Directory Structure

Directory Name | Description
---------------|------------
_definition_ | this directory contains all scheme for specification files
_scheme_ | this directory will contains scheme for _request_ and _response_
_scheme/request_ | this directory will contains scheme for request data
_scheme/model_ | this directory will contains scheme for response data, model structure, or even the database structure specification

## Data Type Definition

Data type used will follow data type specification from [json-schema.org](https://json-schema.org/)

Data Type | Description
----------|------------
_numeric_ | a set of number data type, could be integers or decimals
_string_ | equal to varchar in database data type
_boolean_ | logical data type, could be __true__ or __false__
_array_ | set of array data type
_object_ | value will be fill with other JSON object
_null_ | empty data type

## HTTP Method Definition

Method | Definition
-------|-----------
_GET_ | Used to retrieve resources
_POST_ | Used to create a resource
_PUT_ | Used to update resource if exists or create it instead
_PATCH_ | Used to update resource
_DELETE_ | Used to delete resource

## Service Specification

This section will explain how to use the specification file.

* Specification files will be store in the `webservices/` directory
* The file path inside will represent the web-service uri
* each specification file may contain multiple services depend on _HTTP method_ available for that endpoint
> __For Example :__ `webservices/auth/login-spec.json` read as an endpoint `auth/login`

__Key__ | __Value Type__ | __Description__
--------|---------------|----------------
_name_ | __string__ | Service Name, for labeling purpose only
_endpoint_ | __string__ | Service's url path
_method_ | __string__ | Service's HTTP method
_variables_ | __array__ | Endpoint dynamic variable. Example: `auth/profile/1` read as `auth/profile/{id}` with `id` as variable
_parameters_ | __array__ | url query parameters. Example: `auth/user/?page=10&search=abdullah`
_headers_ | __array__ | Endpoint request's header. Example: `Authorization: Bearer mytoken.123.321`
_request_ | __string__ | path to request scheme
_response_ | __string__ | path to model scheme
_uploads_ | __array__ | if any file upload required specify here the name, allowed file type, allowed extension
_downloads_ | __array__ | if any file download available specify here the name, generated file type, generated extension
_errors_ | __array__ | list available error in the service
